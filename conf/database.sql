
CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `timeout` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `project_item` (
  `id` int(11) NOT NULL,
  `project` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `flag` int(1) NOT NULL,
  `running` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `project` ADD PRIMARY KEY (`id`);
ALTER TABLE `project_item` ADD PRIMARY KEY (`id`);
ALTER TABLE `project` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10001;
ALTER TABLE `project_item` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
