<?php

# 2 Aug 2020
# 1.1

function curl_post( $url , $params=null, $method="POST" ){

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url );
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
	if( sizeof($params) ){
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params) );
	}
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec ($ch);
	curl_close ($ch);

	return $server_output;

}


function curl( $url, $timeout=10 ){

        $ch = curl_init( $url );
        curl_setopt($ch, CURLOPT_HEADER, "Xtream-Codes IPTV Panel Pro");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
}

