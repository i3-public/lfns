<?php


ini_set('display_errors', 'on');
error_reporting(E_ALL & ~E_NOTICE);


$file = '/var/www/.env';

if(! file_exists($file) ){
    die('no .env found.');

} else {

	$file = file($file);
	if( sizeof($file) ){

		foreach( $file as $line ){
		
			if(! $line = trim($line,"\r\t\n ") )
				continue;
				
			if( substr($line, 0, 1) == '#' )
				continue;
				
			if(! strstr($line, '=') )
				continue;

			$pos = strpos($line, '=');
			
			$k = substr($line, 0, $pos);
			$v = substr($line, $pos+1 );

			if( strtolower($v) === 'false' ){
				$v = false;

            } else if( strtolower($v) === 'true' ){
				$v = true;
            
            } else if( strtolower($v) === 'null' ) {
				$v = null;
            }
			
			define($k, $v);

		}
	}
}


foreach( glob('/var/www/inc/*.php') as $file )
	include_once($file);




