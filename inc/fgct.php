<?php

# 1 Aug 2020

function fgct( $url, $timeout=4, $error=false ){
	
	$contx = stream_context_create([

		'http' => [
			'timeout' => intval($timeout),
		],

	    'ssl' => [
	        'verify_peer'=>false,
	        'verify_peer_name'=>false,
	    ],

    ]);

	if( $error ){
		return file_get_contents( $url, false, $contx );
	
	} else {
		return @file_get_contents( $url, false, $contx );
	}
	
}



