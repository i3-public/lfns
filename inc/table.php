<?php

# 4 Aug 2020

/*
$rw_deposit = table('xb_deposit', $deposit_id);
$rw_PI = table('xb_paypal_tnx', [
	'PIU_table' => 'deposit',
	'PIU_ID' => $deposit_id,
	'PIU_commit' => 1,
	'cost' => [ '>' , 0 ],
	], $limit=1 );
*/

function table( $table , $id, $limit=null ){

	$q_limit = '';
	
	if( is_array($id) ){
		
		foreach( $id as $k => $v ){
			
			if(! is_array($v) ){
				if( $v === null ){
					$q_where[] = "`$k` is null";
				} else {
					$q_where[] = "`$k`=".(is_numeric($v)?"$v":"'$v'");
				}
			
			} else if( strtoupper(trim($v[0])) == "IN" ){
				if( is_array($v[1]) ){
					$v[1] = implode(',', $v[1] );
				}
				$q_where[] = "`$k` IN (".$v[1].")";

			} else {
				$q_where[] = "`$k` ".$v[0]."'".$v[1]."'";
			}
		}

		$q_where = implode(' AND ', $q_where);

		if( $limit ){
			$q_limit = "LIMIT $limit";
		}

	} else {
		$q_where = "`id`=$id";
		$q_limit = "LIMIT 1";
	}
	
	if(! $rs = mysql_query(" SELECT * FROM `$table` WHERE $q_where $q_limit ") ){
		echo mysql_error();
		return false;

	} else if(! mysql_num_rows($rs) ){
		return false;

	} else if( is_array($id) and $limit!=1 ){
		while( $rw = mysql_fetch_assoc($rs) ){
			$rw_s[] = $rw;
		}
		return $rw_s;

	} else {
		return mysql_fetch_assoc($rs);
	}

}


function dbq( $q ){
	return mysql_query($q);
}



function dbr($rs, $row, $col=null){

	if( $col ){
		return mysql_result($rs, $row, $col);

	} else {
		return mysql_result($rs, $row);
		
	}
}


function dbc( $table, $where_arr=null ){

	$where = '';

	if( $where_arr ){
		
		foreach( $where_arr as $k => $v ){

			if(! is_array($v) ){
				
				if( $v === null ){
					$where.= " AND `$k` is null ";
				} else {
					$where.= " AND `$k`=".(is_numeric($v)?"$v":"'$v'");
				}
			
			} else if( strtoupper(trim($v[0])) == "IN" ){
				
				if( is_array($v[1]) ){
					$v[1] = implode(',', $v[1] );
				}
				
				$where.= " AND `$k` IN (".$v[1].") ";

			} else {
				$where.= " AND `$k` ".$v[0]." '".$v[1]."' ";
			}

		}
		
	}

	return dbr( dbq(" SELECT COUNT(*) FROM `$table` WHERE 1 $where "), 0, 0);

}


function dbn($rs){
	
	return mysql_num_rows($rs);

}



function dbf($rs){
	
	return mysql_fetch_assoc($rs);

}



function dbaf(){
	
	return mysql_affected_rows();

}



function dbrm( $table, $id ){
	
	$id = intval($id);
	return dbq(" DELETE FROM `$table` WHERE `id`=$id LIMIT 1 ");

}



function dbe(){

	return mysql_error();

}



function dbi(){

	return mysql_insert_id();

}



function dbd(){
	mysql_close();
}












