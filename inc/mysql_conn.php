<?php


function mysql_conn(){

	global $dp;

	if(! $dp = mysqli_connect( MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_NAME ) ){
		echo "Can't connect to mysql server";
		die();
	}

	// $dp->set_charset('utf8');
	// mysqli_query($dp, " SET NAMES utf8 ");

}


function mysql_query( $q, $dp=null ){
	if(! $dp ){
		global $dp;
	}
	// echo $q."<br>";
	if(! $dp ) mysql_conn();
	return mysqli_query($dp, $q);
}


function mysql_num_rows( $rs ){
	return mysqli_num_rows($rs);
}


function mysql_num_fields( $rs ){
	return mysqli_num_fields($rs);
}


function mysql_result($res, $row, $field=0){ 
    $res->data_seek($row); 
    $datarow = $res->fetch_array(); 
    return $datarow[$field]; 
} 


function mysql_fetch_assoc($res){
	return mysqli_fetch_assoc($res);
}


function mysql_fetch_array($res){
	return mysqli_fetch_array($res);
}


function mysql_error(){
	global $dp;
	return mysqli_error($dp);
} 


function mysql_errno(){
	global $dp;
	return mysqli_errno($dp);
} 


function mysql_affected_rows(){
	global $dp;
	return mysqli_affected_rows($dp);
} 


function mysql_insert_id(){
	global $dp;
	return mysqli_insert_id($dp);
}

function mysql_close(){
	global $dp;
	mysqli_close($dp);
}

function mysql_real_escape_string( $str ){
	global $dp;
	return mysqli_real_escape_string($dp, $str);
}

