<?php include_once '/var/www/inc/.php';


$ip = $argv[1];
$id = $argv[2];

if( !$ip or !$id )
	die;


#
# retrive variables
if(! $project_item = table('project_item', $id) )
	die;
extract($project_item);

$timeout = $project > 0
	? table('project', $project)['timeout']
	: 18
	;

# if its vod, make a longer timeout
if( in_array( substr(strrchr($source, "."), 1) , [ 'mkv', 'mp4', 'flv', 'avi', 'mov', 'wmv' ] ) )
	$timeout = 30;



#
# request
$res = fgct("http://$ip/?timeout=$timeout&source=$source", $timeout);
#



$flag = $res == "OK"
	?  1 // online 
	: -1 // offline
	;

dbq(" UPDATE `project_item` SET `running`=0, `flag`=$flag WHERE `id`=$id LIMIT 1 ");
#




