<?php include_once '/var/www/inc/.php';


#
# semaphore
$ps = trim( shell_exec(" ps aux | grep out-vod.php | grep -v /bin/sh | grep -v grep | wc -l ") , " \r\n\t") - 1;
if( $ps ){
	logg( "out: already running .." );
	die();
}
#


#
# config
$sign_id = -2;
#


#
# main loop
while( 1 ){


	#
	# put into gearbox
	$list = fgct( SIGNAL_POINT."/api/feed/lfns/out-vod/feed.php?seconds=" . ( MINUTES_OUTVOD * 60 ) );
	$list = json_decode($list, true);


	#
	# list of current process
	$rs_c = dbq(" SELECT `source` FROM `project_item` WHERE `project`=-2 ");
	$curr = [];
	while( $rw_c = dbf($rs_c) ){
		$curr[] = $rw_c['source'];
	}
	#


	if( sizeof($list) ){
		
		foreach( $list as $source ){
			if(! in_array($source, $curr) ){
				dbq(" INSERT INTO `project_item` (`project`, `source`) VALUES ($sign_id, '$source') ");
				$curr[] = $source;
			}
		}

		logg("vod: ".sizeof($list)." items added");

	} else {
		logg("vod: no feed");
	}
	#


	#
	# etch out of gearbox
	$rw_s = table('project_item', ['project'=>$sign_id, 'flag'=>['IN', '1,-1'] ]);

	if( $rw_s ){
		
		$list = [];

		foreach( $rw_s as $rw ){ extract($rw);
			dbq(" DELETE FROM `project_item` WHERE `id`=$id LIMIT 1 ");
			$list[ $source ] = ( $flag == 1 ? 'ON' : 'OFF' );
		}
		
		curl_post( SIGNAL_POINT."/api/feed/lfns/out-vod/save.php" , [ 'list' => json_encode($list) ] );
		logg( "vod: ".sizeof($list)." items removed" );

	} else {
		logg('vod: no fetch');
	}
	#
	

	#
	# wait
	sleep(2);
	#


}



