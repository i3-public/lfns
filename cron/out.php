<?php include_once '/var/www/inc/.php';


#
# semaphore
$ps = trim( shell_exec(" ps aux | grep out.php | grep -v /bin/sh | grep -v grep | wc -l ") , " \r\n\t") - 1;
if( $ps ){
	logg( "out: already running .." );
	die();
}
#


#
# config
$sign_id = -1;
#


#
# main loop
while( 1 ){


	#
	# put into gearbox
	$json = fgct( SIGNAL_POINT."/api/feed/lfns/out/feed.php?seconds=" . ( MINUTES_OUT * 60 ) );
	$json = json_decode($json, true);
	extract($json);
	#


	#
	# list of current process
	$rs_c = dbq(" SELECT `source` FROM `project_item` WHERE `project`=-1 ");
	$curr = [];
	$new_added = 0;
	while( $rw_c = dbf($rs_c) ){
		$curr[] = $rw_c['source'];
	}
	#
	

	if( sizeof($list) ){
		
		foreach( $list as $id ){
			$source = $line.'/'.$id;
			if(! in_array($source, $curr) ){
				dbq(" INSERT INTO `project_item` (`project`, `source`) VALUES ($sign_id, '$source') ");
				$curr[] = $source;
				$new_added++;
			}
		}

		logg("out: {$new_added} of ".sizeof($list)." items added");

	} else {
		logg("out: no feed");
	}
	#


	#
	# fetch out of gearbox
	$rw_s = table('project_item', ['project'=>$sign_id, 'flag'=>['IN', '1,-1'] ]);

	if( $rw_s ){
		
		$list = [];

		foreach( $rw_s as $rw ){ extract($rw);
			dbq(" DELETE FROM `project_item` WHERE `id`=$id LIMIT 1 ");
			$list[ $source ] = ( $flag == 1 ? 'ON' : 'OFF' );
		}

		curl_post( SIGNAL_POINT."/api/feed/lfns/out/save.php" , [ 'list' => json_encode($list) ] );
		logg( "out: ".sizeof($list)." items removed" );

	} else {
		logg('out: no fetch');
	}
	#
	

	#
	# wait
	sleep(2);
	#


}
