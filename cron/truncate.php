<?php include_once '/var/www/inc/.php';


#
# forget the running projects -- after reboot
if( array_key_exists(1, $argv) and $argv[1] == 'init' ){
    
    logg( "truncate: init .." );
    
    dbq(" UPDATE `project_item` SET `running`=0 ");
    logg( "truncate: ".dbaf()." running items flush" );
    
}
#


#
# delete old filter request
$date_b12h = date('Y-m-d H:i:s', strtotime( date('Y-m-d H:i:s')." -12 hours") );

// remove all projects which are out-dated
dbq(" DELETE FROM `project` WHERE `date` < '$date_b12h' ");
logg( "truncate: ".dbaf()." projects removed" );

// remove all filter requests where the project is not registered
dbq(" DELETE FROM `project_item` WHERE `project` > 0 AND `project` NOT IN (SELECT `id` FROM `project`) ");
logg( "truncate: ".dbaf()." items removed" );
#

# delete old result files
# 
