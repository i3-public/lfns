<?php include_once '/var/www/inc/.php';


#
# semaphore
$ps = trim( shell_exec(" ps aux | grep mix.php | grep -v /bin/sh | grep -v grep | wc -l ") , " \r\n\t") - 1;
if( $ps ){
	// logg( "mix: already running .." );
	die;
}
#


#
# list of lfns
$lfn_s = json_decode( fgct(SIGNAL_POINT."/api/feed/lfns/list_of_lfn.php") );
// $last_update_min = date('i');
#


#
# main loop

$running = [];

while( 1 ){

	// if( $last_update_min != date('i') ){
	// 	$lfn_s = json_decode( fgct(SIGNAL_POINT."/api/feed/lfns/list_of_lfn.php") );
	// 	$last_update_min = date('i');
	// }
	
	#
	# curr proc
	$lfn_s_tmp = $lfn_s;
	$free_on_all = 0;
	$free_on_lb = [];
	foreach( $lfn_s_tmp as $i => $lfn ){

		$current_process = fgct("http://$lfn/?do=current_process", 2);

		if( is_numeric($current_process) ){
	
			// logg( 'mix: '.$lfn."; $current_process / ".FFMPEG_MAX_PROC );

			$free = FFMPEG_MAX_PROC - $current_process;

			if( $free > 0 ){
				$free_on_lb[ $lfn ] = $free;
				$free_on_all+= $free;
			
			} else {
				$free_on_lb[ $lfn ] = 0;
			}

		} else {
			unset($lfn_s_tmp[$i]);
		}

	}
	#


	#
	# main job
	$rs = dbq(" SELECT `id` FROM `project_item` WHERE `flag`=0 AND `running`=0 ORDER BY ( `project` > 0 ) DESC , `id` ASC LIMIT $free_on_all ");
	$rn = dbn($rs);
	
	$i = 0;
	$lfn_s_tmp = $lfn_s;
	$triggred_on_lb = [];
	while( $rw = dbf($rs) ){ extract($rw);

		#
		# find the lfn server
		$lfn = $lfn_s_tmp[ array_keys($lfn_s_tmp)[$i] ];
		#

		# 
		# mark this record as running
		dbq(" UPDATE `project_item` SET `running`=1 WHERE `id`=$id LIMIT 1 ");
		#

		#
		# execute the request
		$command = " php -q /var/www/async-lfn-req.php $lfn $id > /dev/null &";
		logg( 'mix: '.$command );
		exec($command);
		#

		#
		# 
		$triggred_on_lb[ $lfn ]++;
		$free_on_lb[ $lfn ]--;
		if( $free_on_lb[ $lfn ] == 0 ){
			unset($lfn_s_tmp[ $i ]);
		}
		#

		if( sizeof($lfn_s_tmp) > 1 ){
			$i++;
			$i = $i % sizeof($lfn_s_tmp);
			
		} else {
			$i = 0;
		}

	}
	foreach($triggred_on_lb as $ip => $triggered){
		logg("mix: triggered on $ip -> $triggered");
	}
	#


	#
	# loop
	usleep( 1000000 * 0.5 );
	// logg("mix: .");
	#


}
#


