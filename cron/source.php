<?php include_once '/var/www/inc/.php';


#
# semaphore
$ps = trim( shell_exec(" ps aux | grep source.php | grep -v /bin/sh | grep -v grep | wc -l ") , " \r\n\t") - 1;
if( $ps ){
	logg( "source: already running .." );
	die();
}
#


#
# outer-loop config
$sign_id = -3;
#


#
# main loop
while( 1 ){
	
	#
	# put into gearbox
	$list = fgct( SIGNAL_POINT."/api/feed/lfns/source/feed.php?seconds=" . ( MINUTES_SOURCE * 60 ) );
	$list = json_decode($list, true);

	#
	# list of current process
	$rs_c = dbq(" SELECT `source` FROM `project_item` WHERE `project`=-3 ");
	$curr = [];
	while( $rw_c = dbf($rs_c) ){
		$curr[] = $rw_c['source'];
	}
	#

	if( sizeof($list) ){
		
		logg( "source: ". sizeof($list)." items in feed queue\n" );

		foreach( $list as $source => $name ){
			if(! in_array($source, $curr) ){
				dbq(" INSERT INTO `project_item` (`project`, `source`, `name`) VALUES ($sign_id, '$source', '$name') ");
				$curr[] = $source;
			}
		}

		logg("source: ".sizeof($list)." items added");

	} else {
		logg('source: no feed');
	}
	#


	#
	# etch out of gearbox
	$rw_s = table('project_item', ['project'=>$sign_id, 'flag'=>['IN', '1,-1'] ]);

	if( $rw_s and sizeof($rw_s) ){
		
		logg( "source: ". sizeof($rw_s)." items in save queue\n" );

		$list = [];
		$id_to_remove = [];

		foreach( $rw_s as $rw ){ extract($rw);
			$flag = ( $flag == 1 ? 'ON' : 'OFF' );
			$list[ $source ] = [ $name, $flag ];
			$id_to_remove[] = $id;
		}

		$res = curl_post( SIGNAL_POINT."/api/feed/lfns/source/save.php" , [ 'list' => json_encode($list) ] );

		if( $res == 'OK' ){
			dbq(" DELETE FROM `project_item` WHERE `id` IN (".implode(',', $id_to_remove).") ");
			logg( "source: ".sizeof($list)." items removed" );
		}


	} else {
		logg('source: no fetch');
	}
	#
	
	
	#
	# wait
	sleep(2);
	#


}


