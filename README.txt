# 
# main port: 4318
# wget -qO- https://gitlab.com/i3-public/lfns/-/raw/main/README.txt | bash

apt -y update
apt -y upgrade

wget -qO- https://gitlab.com/i3-public/public-text/-/raw/master/preinstall-raw-os | bash

pass=$(echo $RANDOM | md5sum | awk {'print $1'})

wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/mysql-on-docker/README.txt | bash -s $pass
wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/nginx-php/README.txt | bash -s 8.2 lfns 4318 https://gitlab.com/i3-public/lfns/-/raw/main/conf/patch '--net moss'

docker exec lfns bash -c " sed -i 's/MYSQL_PASS=password/MYSQL_PASS="$pass"/g' /var/www/.env "

docker exec lfns bash -c "
echo '[client]
host=172.23.0.33
user=root
password="$pass"' > /root/.my.cnf"

docker exec lfns bash -c " mysql -e 'CREATE DATABASE lfns;' && mysql lfns < /var/www/conf/database.sql "

